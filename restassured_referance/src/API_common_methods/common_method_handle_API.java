package API_common_methods;

import static io.restassured.RestAssured.given;

public class common_method_handle_API {
	
//***-------post----
	public static int Post_statuscode(String Requestbody,String endpoint) {
		
	int statusCode = given().header("Content-Type","application/json").body(Requestbody)
		.when().post(endpoint).then().extract().statusCode();	
	return statusCode;
	
	}
	public static String Post_responseBody(String Requestbody,String endpoint)	{
		
		String responseBody = given().header("Content-Type","application/json").body(Requestbody)
				.when().post(endpoint).then().extract().response().asString()	;
		return responseBody;
	}
		
///***---put------
		public static int Put_statuscode(String Requestbody,String endpoint) {
			
			int statusCode = given().header("Content-Type","application/json").body(Requestbody)
				.when().put(endpoint).then().extract().statusCode();	
			return statusCode;
			
			}
			public static String Put_responseBody(String Requestbody,String endpoint)	{
				
				String responseBody = given().header("Content-Type","application/json").body(Requestbody)
						.when().put(endpoint).then().extract().response().asString()	;
				return responseBody;
			}
				
///***-----patch----***		
	public static int Patch_statuscode(String Requestbody,String endpoint) {
		
		int statusCode = given().header("Content-Type","application/json").body(Requestbody)
			.when().patch(endpoint).then().extract().statusCode();	
		return statusCode;
		
		}
		public static String Patch_responseBody(String Requestbody,String endpoint)	{
			
			String responseBody = given().header("Content-Type","application/json").body(Requestbody)
					.when().patch(endpoint).then().extract().response().asString();
			return responseBody;
		}
		
///***---get----***	
        public static int get_statuscode(String endpoint) {
		
		int statusCode = given().when().get(endpoint).then().extract().statusCode();	
		return statusCode;
		
		}
		public static String get_responseBody(String endpoint)	{
			
			String responseBody = given().when().get(endpoint).then().extract().response().asString();
			return responseBody;	
	}

}
