package restassured_referance;
import io.restassured.RestAssured;
import io.restassured.path.json.JsonPath;
import java.time.LocalDateTime;
import org.testng.Assert;

import static io.restassured.RestAssured.given;

public class Put_reference {

	public static void main(String[] args) {
		// step 1:declare base url
		
		RestAssured.baseURI="https://reqres.in/";
		
		//step 2-configure the request parameter and trigger the api
		String requestbody = "{\r\n"
				+ "    \"name\": \"morpheus\",\r\n"
				+ "    \"job\": \"zion resident\"\r\n"
				+ "}";
		String responsebody = given().header("Content-Type","application/json")
				.body(requestbody)
				.log().all().when().put("api/users/2")
				.then().log().all().extract().response().asString();
		//System.out.println("responsebody is : "+responsebody);
		
		//step 3 create and object of json path to parse the requestbody and responsebody
		JsonPath jsp_req = new JsonPath(requestbody);
		String req_name = jsp_req.getString("name");
		String req_job = jsp_req.getString("job");
		LocalDateTime currentdate = LocalDateTime.now();
		String expecteddate = currentdate.toString().substring(0, 11);
		
		JsonPath jsp_res = new JsonPath(responsebody);
		String res_name = jsp_res.getString("name");
		//String res_id = jsp_res.getString("id");
		String res_job = jsp_res.getString("job");
		String res_createdate = jsp_res.getString("updatedAt");
		res_createdate = res_createdate.substring(0, 11);
		
//Step 4 Validate responsebody
		Assert.assertEquals(res_name, req_name);
		Assert.assertEquals(res_job, req_job);
		//Assert.assertNotNull(res_id);
		Assert.assertEquals(res_createdate, expecteddate);


	}

}


	

