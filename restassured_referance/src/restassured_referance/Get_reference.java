package restassured_referance;
import io.restassured.RestAssured;
import io.restassured.path.json.JsonPath;
import static io.restassured.RestAssured.given;
import org.json.JSONArray;
import org.json.JSONObject;
import org.testng.Assert;
public class Get_reference {

	public static void main(String[] args) {
		
		int expected_id[]= {1,2,3,4,5,6};
		String expected_firstname[] = { "George", "Janet", "Emma", "Eve", "Charles", "Tracey" };
		String expected_lastname[] = { "Bluth", "Weaver", "Wong", "Holt", "Morris", "Ramos" };
		String expected_email[] = { "george.bluth@reqres.in", "janet.weaver@reqres.in", "emma.wong@reqres.in",
				"eve.holt@reqres.in", "charles.morris@reqres.in", "tracey.ramos@reqres.in" };
		
		// step 1:declare base url
		
				RestAssured.baseURI="https://reqres.in";
				
				//step 2-configure the request parameter and trigger the api
				
				String responsebody = given().when().get("api/users?page=1").then().extract().response().asString();
				System.out.println("responsebody is:"+responsebody);
				
				JsonPath jsp_res = new JsonPath(responsebody);
				JSONObject array_res=new JSONObject(responsebody);
				JSONArray dataarray =array_res.getJSONArray("data");
				System.out.println(dataarray);
				int count =dataarray.length();
				System.out.println(count);
				
				for(int i=0;i<count;i++) {
					int res_id=dataarray.getJSONObject(i).getInt("id");
					String res_first_name=dataarray.getJSONObject(i).getString("first_name");
					String res_last_name=dataarray.getJSONObject(i).getString("last_name");
					String res_email=dataarray.getJSONObject(i).getString("email");
		            int exp_id=expected_id[i];
		            System.out.println(res_id);
					String exp_first_name=expected_firstname[i];
					String exp_last_name=expected_lastname[i];
					String exp_email=expected_email[i];
					
					Assert.assertEquals(res_id,exp_id);
					Assert.assertEquals(res_first_name, exp_first_name);
					Assert.assertEquals(res_last_name, exp_last_name);
					Assert.assertEquals(res_email, exp_email);
					
				}
						
				
				
					

	}

}
