package restassured_referance;
import io.restassured.RestAssured;
import io.restassured.path.xml.XmlPath;

import static io.restassured.RestAssured.given;

import org.testng.Assert;
public class SOAP_Reference {

	public static void main(String[] args) {
		//Step1: declare the base URI
		String BaseURI="https://www.dataaccess.com";
		
		//Sep2: declare the resquestbody
		String requestBody="<?xml version=\"1.0\" encoding=\"utf-8\"?>\r\n"
				+ "<soap:Envelope xmlns:soap=\"http://schemas.xmlsoap.org/soap/envelope/\">\r\n"
				+ "  <soap:Body>\r\n"
				+ "    <NumberToWords xmlns=\"http://www.dataaccess.com/webservicesserver/\">\r\n"
				+ "      <ubiNum>500</ubiNum>\r\n"
				+ "    </NumberToWords>\r\n"
				+ "  </soap:Body>\r\n"
				+ "</soap:Envelope>";
		//Step3: trigger the api and fetch the responsebody.
		RestAssured.baseURI=BaseURI;
		String responseBody=given().header("Content-Type", "text/xml; charset=utf-8").body(requestBody)
		.when().post("/webservicesserver/NumberConversion.wso").then().extract()
		.response().getBody().asString();
		
		//step4: print the responsebody
		System.out.println(responseBody);
		
		//Step5: extract the response body parameter.
		XmlPath Xml_res=new XmlPath(responseBody); 
		String res_tag=Xml_res.getString("NumberToWordsResult");
		System.out.println(res_tag);
		
		//step validate the responsebody
		Assert.assertEquals(res_tag, "five hundred ");
	
	}

}
