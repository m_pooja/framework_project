package restassured_referance;
import static io.restassured.RestAssured.given;
import org.testng.Assert;
import io.restassured.RestAssured;
import io.restassured.path.xml.XmlPath;

public class soap_api_reference {

	public static void main(String[] args) {
		// step:1 declare the base url
          String BaseURI="https://www.dataaccess.com";
		
		//Sep2: declare the resquestbody.
          String requestBody="<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:web=\"http://www.dataaccess.com/webservicesserver/\">\r\n"
          		+ "   <soapenv:Header/>\r\n"
          		+ "   <soapenv:Body>\r\n"
          		+ "      <web:NumberToDollars>\r\n"
          		+ "         <web:dNum>623</web:dNum>\r\n"
          		+ "      </web:NumberToDollars>\r\n"
          		+ "   </soapenv:Body>\r\n"
          		+ "</soapenv:Envelope>";
          
        //Step3: trigger the api and fetch the responsebody.
  		RestAssured.baseURI=BaseURI;
  		String responseBody=given().header("Content-Type", "text/xml; charset=utf-8").body(requestBody)
  		.when().post("/webservicesserver/NumberConversion.wso").then().extract()
  		.response().getBody().asString();
  		
  	    //step4: print the responsebody
  			System.out.println(responseBody);
  			
  			//Step5: extract the response body parameter.
  			XmlPath Xml_res=new XmlPath(responseBody); 
  			String res_tag=Xml_res.getString("NumberToDollarsResult");
  			System.out.println(res_tag);
  			//step validate the responsebody
  			Assert.assertEquals(res_tag,"six hundred and twenty three dollars");


	}

}
