package restassured_referance;

import io.restassured.RestAssured;
import io.restassured.path.json.JsonPath;

import static io.restassured.RestAssured.given;

import org.testng.Assert;

public class post_referense_pp {

	public static void main(String[] args) {
		//step1:declare the url
		RestAssured.baseURI="https://reqres.in/";
		
		//step2: configure the req parameter trigger api
		String requestbody ="{\r\n"
				+ "    \"name\": \"morpheus\",\r\n"
				+ "    \"job\": \"leader\"\r\n"
				+ "}\r\n";
		String responsebody = given().header("Content-Type","application/json").body(requestbody).when().post("api/users").then().extract().response().asString();
		
		System.out.println("responsebody:"+responsebody);
		
		//step3:creat an object jsonpath to parse requestbody and responsebody
		
		JsonPath jsp_req =new JsonPath(requestbody);
		String req_name = jsp_req.getString("name");
		String req_job =jsp_req.getString("job");
		
		JsonPath jsp_res =new JsonPath(responsebody);
		String res_name =jsp_res.getString("name");
		String res_job =jsp_res.getString("job");
		
		//step4 validate body
		Assert.assertEquals(req_name, res_name);
		Assert.assertEquals(req_job, res_job);
			
	}

}
